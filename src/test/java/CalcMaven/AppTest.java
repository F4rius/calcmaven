package CalcMaven;

import static CalcMaven.CalcFromFile.toOperationAndValues;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.ArrayList;

public class AppTest {

    @Test
    public void toOperationAndValues_method_returnsCorrectOperationsAndValues() {
        ArrayList<String> actualList = new ArrayList<>();
        actualList.add("add 4");
        actualList.add("multiply 6");
        actualList.add("apply 10");

        ArrayList<Instruction> expectedList = new ArrayList<>();
        expectedList.add(new Instruction("add", 4));
        expectedList.add(new Instruction("multiply", 6));
        expectedList.add(new Instruction("apply", 10));

        for (int i = 0; i < actualList.size(); i++) {
            assertEquals(toOperationAndValues(actualList).get(i).getValue(), expectedList.get(i).getValue());
            assertEquals(toOperationAndValues(actualList).get(i).getOperation(), expectedList.get(i).getOperation());
        }

    }

    @Test
    public void calculate_method_returnsCorrectValue(){
        ArrayList<Instruction> list = new ArrayList<>();
        CalcFromFile app = new CalcFromFile();
        list.add(new Instruction("add", 4));
        list.add(new Instruction("multiply", 3));
        list.add(new Instruction("apply", 10));

        assertEquals(42.0, app.calculate(list),0.0000001);

        ArrayList<Instruction> list2 = new ArrayList<>();
        list2.add(new Instruction("add", 4));
        list2.add(new Instruction("multiply", 3));
        list2.add(new Instruction("subtract", 12));
        list2.add(new Instruction("apply", 10));

        assertEquals(30.0,app.calculate(list2),0.0000001);

        ArrayList<Instruction> list3 = new ArrayList<>();
        list3.add(new Instruction("add", 4));
        list3.add(new Instruction("multiply", 3));
        list3.add(new Instruction("subtract", 12));
        list3.add(new Instruction("divide", 3));
        list3.add(new Instruction("apply", 10));

        assertEquals(10.0,app.calculate(list3),0.0000001);
    }

}
