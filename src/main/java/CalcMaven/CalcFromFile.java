package CalcMaven;

import java.io.*;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalcFromFile {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalcFromFile.class);

    public static void main(String[] args) {
        CalcFromFile app = new CalcFromFile();
        ArrayList<String> instructions;

        try {
            instructions = app.fileToArrayList("file.txt");
            System.out.println(app.calculate(toOperationAndValues(instructions)));
        } catch (NullPointerException e) {
            LOGGER.error("File not found");
            e.printStackTrace();
        }
    }

    //Get file from resources folder and convert to list of Strings
    public ArrayList<String> fileToArrayList(String fileName) {

        ArrayList<String> instructions = new ArrayList<>();

        try {
            InputStream input = getClass().getResourceAsStream("../" + fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String sCurrentLine;

            while ((sCurrentLine = reader.readLine()) != null) {
                instructions.add(sCurrentLine);
            }

        } catch (IOException e) {
            LOGGER.error("IOException");
            e.printStackTrace();
        }

        return instructions;
    }

    //do mathematical operations operations
    public double calculate(ArrayList<Instruction> list) {

        double result = (double) list.get(list.size() - 1).getValue();

        for (int i = 0; i < list.size(); i++){
            switch (list.get(i).getOperation()){
                case "add":
                    result += list.get(i).getValue();
                    break;
                case "multiply":
                    result *= list.get(i).getValue();
                    break;
                case "divide":
                    result /= list.get(i).getValue();
                    break;
                case "subtract":
                    result -= list.get(i).getValue();
                    break;

            }
        }
        return result;
    }

    //convert String into operation and value
    public static ArrayList<Instruction> toOperationAndValues(ArrayList<String> list){
        ArrayList<Instruction> instructionList = new ArrayList<>();
        Instruction instruction;

        for (String line: list) {

            instruction = new Instruction();

            instruction.setOperation(line.split("\\u0020")[0]);
            try {
                instruction.setValue(Integer.parseInt(line.split("\\u0020")[1]));
            } catch (NumberFormatException e) {
                LOGGER.error("Wrong input format");
                e.printStackTrace();
                return null;
            }

            instructionList.add(instruction);
        }

        return instructionList;
    }
}
